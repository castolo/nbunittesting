#include "Test.h"
#include "Maths.h"

void testFactorial() {
    //checking expression with user-defined message
    REQUIRE(1 == factorial(0), "Forgot to add the base case 0");
    //checking expressions with no messages
    REQUIRE(1 == factorial(1));
    REQUIRE(1 == factorial(0));
    REQUIRE(120 == factorial(5));
    REQUIRE(3628800 == factorial(10));
    //checking that exception was thrown
    try {
        factorial(-1);
        REQUIRE(false, "Exception not thrown");
    } catch (...) {
    }
}

//we cannot overload functions here; each must have its own unique name
int testFactorial2(int x, int y) {
    REQUIRE(x == factorial(y));
    return 0;
}

int main() {
    Test::startSuite("Math Suite");
    Test::testFunction("TestFac1", testFactorial);
    //we can test non-void funcctions, as well as those with parameters
    Test::testFunction("TestFac2", testFactorial2, 1, 1);
    Test::endSuite();
    return 0;
}

