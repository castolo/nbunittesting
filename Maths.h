#ifndef MATHS_H
#define	MATHS_H

#include <stdexcept>

static int factorial(int n) {

    if (n < 0) {
        throw std::domain_error("Factorial does not support negative numbers");
    }
    if (n <= 1) {
        return 1;
    }
    return n * factorial(n - 1);

}



#endif	/* MATHS_H */

