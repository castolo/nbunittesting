# README #

This project is really a single header file ```Test.h``` that serves as a simplified interface to C++ Simple Tests, provided by the Netbeans IDE.

### Structure ###

The header file provides three methods: ```startSuite```, ```endSuite``` and ```testFunction```, which are used to start and end test suites, and test individual functions respectively. Also provided is the assert macro ```REQUIRE```, which accepts an optional message argument to be displayed if the expression being checked evaluates to false.

One workaround that may need to be implemented is that regarding exceptions being thrown. **The code is structured so that any function that throws an exception (or integer error code) fails the test.** If the function is expected to throw an exception, then this can be hacked together in the following way:


```
#!c++

    try {
        std::vector<int> vec;
        int s = vec.at(2);
        REQUIRE(false, "Exception not thrown");
    } catch (...) {
        //exception was thrown, so we're good  
    }
    // ...
```


On a final note, the header file also defines the ```DEBUG``` flag, if it is not already set, so that any assert calls in the code itself are executed.

### Setup ###

To get started, first add the header file ```Test.h``` to the project. Then right-click on the logical *Test Files* folder and select *New C++ Simple Test...*

Once the test cases have been added to the newly created file, right-click on either the project node or newly-created test folder and select *Test* (Alt+F6). Once the tests have been run, the results window will appear, indicating the tests that have passed and failed.

**NB: Occasionally, Netbeans will confuse the directories of your application with the test application. Always make sure that the output directory of the linker is ```${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/<projectname>```**

### Example Usage ###

Let's say we have a file ```Maths.h``` with a function that we wish to test:


```
#!c++
//Maths.h
int factorial(int n) {

    if (n < 0) {
        throw std::domain_error("Factorial does not support negative numbers");
    }
    if (n <= 1) {
        return 1;
    }
    return n * factorial(n - 1);

}

```

Our test file might then look something like this:


```
#!c++

#include "Test.h"
#include "Maths.h"

void testFactorial() {
    //checking expression with user-defined message
    REQUIRE(1 == factorial(0), "Forgot to add the base case 0");
    //checking expressions with no messages
    REQUIRE(1 == factorial(1));
    REQUIRE(1 == factorial(0));
    REQUIRE(120 == factorial(5));
    REQUIRE(3628800 == factorial(10));
    //checking that exception was thrown
    try {
        factorial(-1);
        REQUIRE(false, "Exception not thrown");
    } catch (...) {
    }
}

//we cannot overload functions here; each must have its own unique name
int testFactorial2(int x, int y) {
    REQUIRE(x == factorial(y));
    return 0;
}

int main() {
    Test::startSuite("MathSuite");
    Test::testFunction("TestFac1", testFactorial);
    //we can test non-void functions, as well as those with parameters
    Test::testFunction("TestFac2", testFactorial2, 1, 1);
    Test::endSuite();
    return 0;
}

```


### Author ###

* Steve James (SD.James@outlook.com)